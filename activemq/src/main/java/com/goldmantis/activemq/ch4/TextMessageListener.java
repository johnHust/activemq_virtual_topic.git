package com.goldmantis.activemq.ch4;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class TextMessageListener implements MessageListener {

	public void onMessage(Message message) {
		try {
			TextMessage map = (TextMessage)message;
			System.out.println(map.getText());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
