package com.goldmantis.activemq.ch4;

import java.util.Hashtable;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class TextMessagePublisher {
	
    protected int MAX_DELTA_PERCENT = 1;
    protected Map<String, Double> LAST_PRICES = new Hashtable<String, Double>();
    protected static int count = 10;
    protected static int total;
    
    protected static String brokerURL = "tcp://localhost:31616";
    protected static transient ConnectionFactory factory;
    protected transient Connection connection;
    protected transient Session session;
    protected transient MessageProducer producer;
    
    public TextMessagePublisher() throws JMSException {
    	factory = new ActiveMQConnectionFactory(brokerURL);
    	connection = factory.createConnection();
    	try {
        connection.start();
    	} catch (JMSException jmse) {
    		connection.close();
    		throw jmse;
    	}
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        producer = session.createProducer(null);
        //设置传递模式，是否持久化
        //producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
    }
    
    public void close() throws JMSException {
        if (connection != null) {
            connection.close();
        }
    }
    
    public static void main(String[] args) throws JMSException {
        TextMessagePublisher publisher = new TextMessagePublisher();
        publisher.sendMessage();
        publisher.close();
    }

    protected void sendMessage() throws JMSException {
    	Destination destination = session.createTopic("STOCKS.1");
        while (total++ < 1000) {
            
            Message message = createStockMessage(session,total);
            producer.send(destination, message);
            System.out.println("Published '" +total + "' price messages");
            try {
              Thread.sleep(1000);
            } catch (InterruptedException x) {
            }
        }
    }

    protected Message createStockMessage(Session session,int num) throws JMSException {
		TextMessage message = session.createTextMessage();
		message.setText(String.format("第%d次say:%s",num,"hello world"));
		return message;
    }
}
