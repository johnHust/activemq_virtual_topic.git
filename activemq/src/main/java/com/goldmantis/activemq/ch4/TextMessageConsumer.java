package com.goldmantis.activemq.ch4;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

public class TextMessageConsumer {

    private static String brokerURL = "tcp://localhost:31616?trace=true";
    private static transient ConnectionFactory factory;
    private transient Connection connection;
    private transient Session session;
    
    public TextMessageConsumer() throws JMSException {
    	factory = new ActiveMQConnectionFactory(brokerURL);
    	connection = factory.createConnection();
    	connection.setClientID("topicConsumer1");
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }
    
    public void close() throws JMSException {
        if (connection != null) {
            connection.close();
        }
    }    
    
    public static void main(String[] args) throws JMSException {
    	TextMessageConsumer consumer = new TextMessageConsumer();
		Destination destination = consumer.getSession().createTopic("STOCKS.1");
		//创建持久化订阅
		//consumer.getSession().createDurableSubscriber((Topic)destination,"test");
		//创建非持久化订阅
		MessageConsumer messageConsumer = consumer.getSession().createConsumer(destination);//
		messageConsumer.setMessageListener(new TextMessageListener());
    }
	
	public Session getSession() {
		return session;
	}

}
