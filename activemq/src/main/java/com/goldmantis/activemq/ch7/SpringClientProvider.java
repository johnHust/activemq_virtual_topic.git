package com.goldmantis.activemq.ch7;

import org.springframework.context.support.ClassPathXmlApplicationContext;


public class SpringClientProvider {

	public static void main(String[] args) throws Exception {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-client-provider.xml");
		SpringPublisher publisher = (SpringPublisher)context.getBean("stockPublisher");
		publisher.start();
	}

}
