package com.goldmantis.activemq.ch3.queue;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

public class MQQueueSessionConsumer {

    private static String brokerURL = "tcp://localhost:31616";
    private static transient ActiveMQConnectionFactory factory;
    private transient ActiveMQConnection connection;
    private transient Session session;
    
    private String jobs[] = new String[]{"suspend", "delete"};
    
    public MQQueueSessionConsumer() throws JMSException {
    	factory = new ActiveMQConnectionFactory(brokerURL);
    	factory.setDispatchAsync(false);
    	connection = (ActiveMQConnection)factory.createConnection();
    	//connection.setDispatchAsync(false);
    	//connection.setClientID("consumer2");
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }
    
    public void close() throws JMSException {
        if (connection != null) {
            connection.close();
        }
    }    
    
    public static void main(String[] args) throws JMSException {
    	MQQueueSessionConsumer consumer = new MQQueueSessionConsumer();
    	//使用consumer.prefetchSize=100参数来控制broker一次最多发送给consumer的消息个数，consumer.prefetchSize=0为一种特例
		Queue destination = new ActiveMQQueue("JOBS.1");//consumer.getSession().createQueue("JOBS.1");
		MessageConsumer messageConsumer = consumer.getSession().createConsumer(destination);
		while(true){
			Message msg = messageConsumer.receive(1000);
			if(msg != null){
				System.out.println("Consumer接收到id:" + ((TextMessage)msg).getText());
				//consumer.getSession().unsubscribe("JOBS.1");
			}else{
	            try {
	                Thread.sleep(1000);
	              } catch (InterruptedException x) {
	              }				
			}		
		}
		//messageConsumer.setMessageListener(new Listener("1"));
    }
	
	public Session getSession() {
		return session;
	}
}
