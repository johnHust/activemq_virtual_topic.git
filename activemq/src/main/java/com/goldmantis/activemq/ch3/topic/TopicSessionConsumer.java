package com.goldmantis.activemq.ch3.topic;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

public class TopicSessionConsumer {

    private static String brokerURL = "tcp://localhost:31616?trace=true";
    private static transient ConnectionFactory factory;
    private transient ActiveMQConnection connection;
    private transient Session session;
    
    public TopicSessionConsumer() throws JMSException {
    	factory = new ActiveMQConnectionFactory(brokerURL);
    	connection = (ActiveMQConnection)factory.createConnection();
    	connection.setClientID("topicConsumer1");
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }
    
    public void close() throws JMSException {
        if (connection != null) {
            connection.close();
        }
    }    
    
    public static void main(String[] args) throws JMSException {
    	TopicSessionConsumer consumer = new TopicSessionConsumer();
		Destination destination = consumer.getSession().createTopic("STOCKS.1");
		MessageConsumer messageConsumer = consumer.getSession().createDurableSubscriber((Topic)destination,"test");
		messageConsumer.setMessageListener(new Listener());
    }
	
	public Session getSession() {
		return session;
	}

}
