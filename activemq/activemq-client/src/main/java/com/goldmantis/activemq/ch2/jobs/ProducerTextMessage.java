package com.goldmantis.activemq.ch2.jobs;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

public class ProducerTextMessage {

    private static String brokerURL = "tcp://localhost:51616?jms.useAsyncSend=false";
    private static transient ConnectionFactory factory;
    private transient Connection connection;
    private transient Session session;
    private transient MessageProducer producer;
    
    private static int count = 1;
    private static int total;
    private static int id = 1000000;
    
    private String jobs[] = new String[]{"suspend"};
    
    public ProducerTextMessage() throws JMSException {
    	factory = new ActiveMQConnectionFactory(brokerURL);
    	connection = factory.createConnection();
    	((ActiveMQConnection)connection).setUseAsyncSend(false);
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        producer = session.createProducer(null);
        producer.setDisableMessageID(true);
        //producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
    }    
    
    public void close() throws JMSException {
        if (connection != null) {
            connection.close();
        }
    }    
    
	public static void main(String[] args) throws JMSException {
    	ProducerTextMessage producer = new ProducerTextMessage();
        for (int i = 0; i < count; i++) {
            producer.sendMessage();
            System.out.println("Sent '" + count + "' of '" + total + "' job messages");
            try {
              Thread.sleep(1000);
            } catch (InterruptedException x) {
            }                
        }
        producer.close();

	}
	
    public void sendMessage() throws JMSException {
        int idx = 0;
        while (true) {
            idx = (int)Math.round(jobs.length * Math.random());
            if (idx < jobs.length) {
                break;
            }
        }
        String job = jobs[idx];
        Destination destination = session.createQueue("JOBS." + job);
        Message message = session.createTextMessage(String.format("%d",id++));
        //message.setJMSDeliveryMode(DeliveryMode.NON_PERSISTENT);
        //message.setJMSCorrelationID(message.getJMSMessageID());
        message.setJMSPriority(5);
        message.setStringProperty("sign", "helloworld");
        System.out.println("Sending: id: " + ((TextMessage)message).getText() + " on queue: " + destination);
        producer.send(destination, message);
    }	

}
