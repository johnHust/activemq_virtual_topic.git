package com.goldmantis.activemq.ch3.queue;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSession;

public class MQQueueSessionLocalPublisher {

    private static String brokerURL = "tcp://localhost:51616";
    private static  ConnectionFactory factory;
    private Connection connection;
    private ActiveMQSession session;
    private MessageProducer producer;
    private MessageConsumer consumer;
    private Queue destination;
    
    private static int count = 10;
    private static int total;
    private static int id = 1000000;
    
    private String jobs[] = new String[]{"suspend", "delete"};
    
    public MQQueueSessionLocalPublisher() throws JMSException {
    	factory = new ActiveMQConnectionFactory(brokerURL);
    	connection = factory.createConnection();
    	//connection.setClientID("clientID");
        connection.start();
        session = (ActiveMQSession)connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        //consumer.noLocal：默认值为true，意味着不接收同一connection下的消息
        destination = session.createQueue("JOBS.1?consumer.noLocal=false");
        producer = session.createSender(destination);
        consumer = session.createReceiver(destination);
        //producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        //producer.setTimeToLive(5000);
    }    
    
    public void close() throws JMSException {
        if (connection != null) {
            connection.close();
        }
    }    
    
    public  void test() throws JMSException{
    	consumer.setMessageListener(new MessageListener(){
    		@Override
    		public void onMessage(Message message){
    			try {
					System.out.println("接收到消息:"+((TextMessage)message).getText());
				} catch (JMSException e) {
				}
    		}
    	});
    	
        while (total < count) {
        	sendMessage();
            total ++;
            //System.out.println("Published '"+total + "' job messages");
            try {
              Thread.sleep(1000);
            } catch (InterruptedException x) {
            }
          }
        close();    	
    }
    
	public static void main(String[] args) throws JMSException {
    	MQQueueSessionLocalPublisher publisher = new MQQueueSessionLocalPublisher();
    	publisher.test();
	}
	
    public void sendMessage() throws JMSException {
        Message message = session.createTextMessage(String.format("%d",id++));
        //Date date = new Date();
        //message.setJMSExpiration(date.getTime()+5000);
        //System.out.println("Sending: id: " + ((ObjectMessage)message).getObject() + " on queue: " + destination);
        producer.send(destination, message);
    }	

}
