package com.goldmantis.activemq.ch3.queue;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSession;
import org.apache.activemq.advisory.AdvisorySupport;
import org.apache.activemq.command.ActiveMQMessage;
import org.apache.activemq.command.ConnectionInfo;
import org.apache.activemq.command.ConsumerInfo;
import org.apache.activemq.command.DataStructure;
import org.apache.activemq.command.RemoveInfo;

public class MQQueueSessionPublisher {

    private static String brokerURL = "tcp://localhost:51616";
    private static transient ConnectionFactory factory;
    private transient Connection connection;
    private transient ActiveMQSession session;
    private transient MessageProducer producer;
    private transient Queue destination;
    
    private static int count = 10;
    private static int total;
    private static int id = 1000000;
    
    private String jobs[] = new String[]{"suspend", "delete"};
    
    public MQQueueSessionPublisher() throws JMSException {
    	factory = new ActiveMQConnectionFactory(brokerURL);
    	connection = factory.createConnection();
    	//connection.setClientID("clientID");
        connection.start();
        session = (ActiveMQSession)connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue("JOBS.1");
        producer = session.createSender(destination);
        //producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        //producer.setTimeToLive(5000);
    }    
    
    public void close() throws JMSException {
        if (connection != null) {
            connection.close();
        }
    }    
    
	public static void main(String[] args) throws JMSException {
    	MQQueueSessionPublisher publisher = new MQQueueSessionPublisher();
    	publisher.sendMessage();
        while (total < count) {
        	publisher.sendMessage();
            total ++;
            System.out.println("Published '"+total + "' job messages");
            try {
              Thread.sleep(5000);
            } catch (InterruptedException x) {
            }
          }
        publisher.close();

	}
	
    public void sendMessage() throws JMSException {
        Message message = session.createTextMessage(String.format("%d",id++));
        //Date date = new Date();
        //message.setJMSExpiration(date.getTime()+5000);
        //System.out.println("Sending: id: " + ((ObjectMessage)message).getObject() + " on queue: " + destination);
        producer.send(destination, message);
    }	

}
