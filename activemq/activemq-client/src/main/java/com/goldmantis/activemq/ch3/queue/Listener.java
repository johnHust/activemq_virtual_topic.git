package com.goldmantis.activemq.ch3.queue;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

public class Listener implements MessageListener {

	private String job;
	
	public Listener(String job) {
		this.job = job;
	}

	public void onMessage(Message message) {
		try {
			//do something here
			//System.out.println(job + " id:" + ((ObjectMessage)message).getObject());
			message.acknowledge();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
