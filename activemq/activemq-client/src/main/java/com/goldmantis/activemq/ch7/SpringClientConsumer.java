package com.goldmantis.activemq.ch7;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.listener.DefaultMessageListenerContainer;


public class SpringClientConsumer {

	public static void main(String[] args) throws Exception {	
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-client-consumer.xml");
		DefaultMessageListenerContainer publisher = (DefaultMessageListenerContainer)context.getBean("orclConsumer");
		publisher.getDestination();
	}

}
