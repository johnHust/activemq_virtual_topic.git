package com.goldmantis.activemq.ch3.topic;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class TopicTextMessageListener implements MessageListener {

	public void onMessage(Message message) {
		try {
			TextMessage map = (TextMessage)message;
			System.out.println(map.getText());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
