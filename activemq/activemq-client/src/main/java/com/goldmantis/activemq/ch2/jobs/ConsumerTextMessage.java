package com.goldmantis.activemq.ch2.jobs;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class ConsumerTextMessage implements MessageListener{

    private static String brokerURL = "tcp://localhost:51616";
    private static transient ConnectionFactory factory;
    private transient Connection connection;
    private transient Session session;
    
    private String jobs[] = new String[]{"suspend"};
    
    public ConsumerTextMessage() throws JMSException {
    	factory = new ActiveMQConnectionFactory(brokerURL);
    	connection = factory.createConnection();
    	connection.setClientID("queueConsumer");
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }
    
    public void close() throws JMSException {
        if (connection != null) {
            connection.close();
        }
    }    
    
    public static void main(String[] args) throws JMSException {
    	ConsumerTextMessage consumer = new ConsumerTextMessage();
		Destination destination = consumer.getSession().createQueue("Q.REQ");
		MessageConsumer messageConsumer = consumer.getSession().createConsumer(destination);
		//ObjectMessage msg = (ObjectMessage)messageConsumer.receive();
/*    		System.out.println(msg.getObject());
		while(msg != null){
			msg = (ObjectMessage)messageConsumer.receive();
    		System.out.println(msg.getObject());
		}*/
		messageConsumer.setMessageListener(consumer);
    }
    
	public void onMessage(Message message) {
		try {
			//do something here
			System.out.println(((TextMessage)message).getText());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}    
	
	public Session getSession() {
		return session;
	}


}
