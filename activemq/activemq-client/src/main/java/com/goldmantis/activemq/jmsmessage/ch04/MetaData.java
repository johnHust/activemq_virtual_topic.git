package com.goldmantis.activemq.jmsmessage.ch04;

import java.util.Enumeration;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.ConnectionMetaData;
import javax.jms.JMSException;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

public class MetaData {

    private static String brokerURL = "tcp://localhost:51616";
    private static transient ConnectionFactory factory;
    private transient Connection connection;
    private transient Session session;
    
    public static void main(String[] args) {    	
    	try {
			// Connect to the provider and get the JMS connection
        	factory = new ActiveMQConnectionFactory(brokerURL);
        	Connection qConnect = factory.createConnection();
			ConnectionMetaData metadata = qConnect.getMetaData();
			System.out.println("JMS Version:  " + metadata.getJMSMajorVersion() + "." + metadata.getJMSMinorVersion());
			System.out.println("JMS Provider: " + metadata.getJMSProviderName());
			System.out.println("JMS Provider Version: " + metadata.getProviderVersion());
			System.out.println("JMSX Properties Supported: ");
			Enumeration e = metadata.getJMSXPropertyNames();
			while (e.hasMoreElements()) {
				System.out.println("   " + e.nextElement());
			}
			qConnect.close();
		} catch (JMSException jmse) {
			jmse.printStackTrace( ); 
			System.exit(1);
		}
    }
	
}
