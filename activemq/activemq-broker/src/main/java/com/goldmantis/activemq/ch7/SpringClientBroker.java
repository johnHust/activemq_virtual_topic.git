package com.goldmantis.activemq.ch7;

import org.apache.activemq.broker.BrokerService;


public class SpringClientBroker {

	public static void main(String[] args) throws Exception {
		BrokerService broker = new BrokerService();
		broker.addConnector("tcp://localhost:51616?maximumConnections=100");
		broker.setPersistent(false);
		broker.setBrokerName("tangyuan");
		broker.setBrokerId("tangyuanBrokerId");
		broker.start();
	}
}
